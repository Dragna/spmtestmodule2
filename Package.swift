// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SPMTestModule2",
    platforms: [.iOS(.v12)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SPMTestModule2",
            targets: ["SPMTestModule2"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/ReactiveX/RxSwift.git", .upToNextMajor(from: "5.1.1")),
        .package(url: "https://github.com/Alamofire/Alamofire", .upToNextMajor(from: "5.2.2")),
        .package(url: "https://github.com/RNCryptor/RNCryptor", .upToNextMajor(from: "5.1.0")),
        .package(url: "https://github.com/SwiftyBeaver/SwiftyBeaver", .upToNextMajor(from: "1.9.2")),
        .package(name: "KeychainSwift", url: "https://github.com/evgenyneu/keychain-swift", from: "19.0.0"),
        .package(name: "SPMTestModule1", url: "https://gitlab.com/Dragna/spmtestmodule1", .branch("master")),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "SPMTestModule2",
            dependencies: [.product(name: "RxSwift", package: "RxSwift"),
                           .product(name: "SPMTestModule1", package: "SPMTestModule1"),
                           "Alamofire",
                           "RNCryptor",
                           "SwiftyBeaver",
                           "KeychainSwift",],
            path: "Sources/SPMTestModule2"),
    ]
)
